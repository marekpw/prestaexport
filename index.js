var chalk = require('chalk');


var prompt = require('prompt');

var schema = {
    properties: {
        hostname: {
            description: 'Hostname k databázovému serveru',
            required: true
        },
        username: {
            description: 'Uživatelské jméno k databázovému serveru',
            default: 'root'
        },
        password: {
            description: 'Heslo k databázovému serveru'
        },
        database: {
            description: 'Název databáze s Prestou',
            required: true
        },
        prefix: {
            description: 'Prefix pro Prestu',
            default: 'ps_'
        },

        output: {
            description: 'Výstupní soubor',
            required: true,
            default: '/tmp/products.json'
        }
    }
};

prompt.get(schema, function (err, result) {
    if (err) {
        throw err;
    } else {
        var products = require('./src/export').export({
            host: result.hostname,
            user: result.username,
            password: result.password,
            database: result.database
        }, result.prefix, result.output).then(function() {
            process.exit();
        }).catch(function(e) {
            console.log('\n'+ e);
            console.log(chalk.red.inverse.bold('\nDošlo k chybě'));
            process.exit(1);
        });

        return products.toJSON();
    }
});
/*
require('./src/export').export({
    host: 'slunce',
    user: 'root',
    password: '',
    database: 'czechoseeds_presta'
}, 'aeroponik_', '/tmp/products.json').then(function() {
    process.exit();
});*/