Jednoduchy script pro vyexportovani dat z Prestashopu.

Spageta, seskladano rychle pro konkretniho klienta. Muze byt nutno upravit kod pro konkretni verzi Presty v zavislosti na zmenach struktury DB.

Plne funkcni na preste 1.6.0.11.

Spustit pres node index.js a nasledovat konfiguracni moznosti

Pozn.: Kod by se mohl zlepsit - vse je nyni reseno pres Promise, protoze Knex neumi pracovat se synchronnimi dotazy, ktere jsou potreba kvuli zapisu do souboru na disk. V budoucnu planuji prepsat nastroj do TS a zvolit lepsi query builder, ktery zvlada i synchronni dotazy na DB.