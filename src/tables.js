
tableReplacers = {

};

var prefixTable = function(name, prefix) {
    return prefix + name;
};

module.exports = function(prefix) {
    return {
        get: function(name, alias) {
            var prefixed = '';

            if (name in tableReplacers) {
                prefixed = prefixTable(tableReplacers[name], prefix);
            }

            prefixed = prefixTable(name, prefix);

            if (alias) {
                return prefixed + ' as ' + alias;
            }

            return prefixed;
        }
    }
};