// vyexportuje data o produktu
var tables = require('../tables');
var _ = require('lodash');
var chalk = require('chalk');

var schema = {
    product: {
        langableColumns: [
            'description',
            'description_short',
            'name',
            'available_now',
            'available_later'
        ],

        internalColumns: [
            'product_link_rewrite',
            'category_link_rewrite',
            'lang_code'
        ]
    },

    attribute: {
        internalColumns: [
            'id_product'
        ]
    },
    image: {
        internalColumns: [
            'id_image',
            'id_product',
            'domain'
        ]
    },

    category: {
        internalColumns: [
            'id_product',
            'id_category'
        ]
    }
};

module.exports = function(knex, prefix) {
    tables = tables(prefix);

    console.log(chalk.cyan.bold('\nNačítám základní informace o produktech...'));

    // data buffer
    var container = [];

    var main = knex.select(_.flatten([
            knex.column('p.id_product').as('id'),
            'p.ean13', 'p.id_category_default', 'p.minimal_quantity', 'p.price', 'p.wholesale_price', 'p.reference',
            'p.supplier_reference', 'p.width', 'p.height', 'p.depth', 'p.weight', 'p.out_of_stock', 'p.active',
            'p.available_for_order', 'p.condition', 'p.show_price', 'p.date_add', 'p.date_upd',
            knex.column('pl.link_rewrite').as('product_link_rewrite'),
            knex.column('cl.link_rewrite').as('category_link_rewrite'),
            schema.product.langableColumns.map(function(item) {
                return 'pl.' + item;
            }),
            knex.column('l.iso_code').as('lang_code')
        ]))
        .from(tables.get('product', 'p'))
        .leftJoin(tables.get('product_lang', 'pl'), 'p.id_product', 'pl.id_product')
        .innerJoin(tables.get('lang', 'l'), 'pl.id_lang', 'l.id_lang')
        .innerJoin(tables.get('category_lang', 'cl'), function() {
            this.on('cl.id_category', 'p.id_category_default')
                .on('cl.id_lang', 'l.id_lang')
        });


    return main.then(function(rows) {

        var total = 0;

        _.each(rows, function(row) {
            var id = row.id;

            var url = row.lang_code + '/' + row.category_link_rewrite + '/' + row.id + '-' + row.product_link_rewrite + '.html';

            var existing = _.findIndex(container, function(product) {
                return product.id == id;
            });

            if (existing !== -1) {
                _.each(schema.product.langableColumns, function(column) {
                    container[existing][column][row.lang_code] = row[column];
                });

                container[existing].url[row.lang_code] = url;
            } else {
                var product = {url:{}};
                total++;

                _.each(row, function(value, column) {
                    if (_.includes(schema.product.langableColumns, column)) {
                        product[column] = {};
                        product[column][row.lang_code] = value;
                    } else if (!_.includes(schema.product.internalColumns, column)) {
                        product[column] = value;
                    }
                });

                product.url[row.lang_code] = url;

                container.push(product);
            }
        });

        console.log(chalk.green('OK - Načteno celkem ' + total + ' položek'));

    }).then(function() {
        var productIds = _.map(container, 'id');

        console.log(chalk.cyan('Načítám kombinace atribut...'));

        return knex.select(_.flatten([
                knex.column('pa.id_product_attribute').as('id'),
                'pa.id_product', 'pac.id_attribute', 'pa.reference', 'pa.supplier_reference',
                'pa.ean13', 'pa.price', 'pa.wholesale_price', 'pa.quantity', 'pa.weight', 'pa.minimal_quantity'
            ]))
            .from(tables.get('product_attribute', 'pa'))
            .innerJoin(tables.get('product_attribute_combination', 'pac'), 'pac.id_product_attribute', 'pa.id_product_attribute')
            .whereIn('pa.id_product', productIds);
    }).then(function(rows) {

        console.log(chalk.yellow('Přiřazuji atributy k položkám...'));

        var total = 0;

        _.each(rows, function (row) {
            var productId = row.id_product;
            var attributeId = row.id;

            var index = _.findIndex(container, function(product) {
                return product.id == productId;
            });

            if (index === -1) {
                throw "Atributa " + attributeId + ": Nelze najít položku s ID " + row.id_product;
            }

            container[index].attributes = container[index].attributes || [];

            var attribute = {};
            total++;

            _.each(row, function(value, column) {
                if (!_.includes(schema.attribute.internalColumns, column)) {
                    attribute[column] = value;
                }
            });

            container[index].attributes.push(attribute);
        });

        console.log(chalk.green('OK - Přiřazeno celkem ' + total + ' atribut'));
    }).then(function() {
        var productIds = _.map(container, 'id');

        console.log(chalk.cyan('Načítám obrázky položek...'));

        return knex.select([
                knex.column('i.id_image').as('id'),
                'i.id_product', 'u.domain', 'i.cover', 'i.position'
            ])
            .from(tables.get('image', 'i'))
            .innerJoin(tables.get('shop_url', 'u'))
            .whereIn('i.id_product', productIds)
            .orderBy('cover', 'desc');

    }).then(function(rows) {
        console.log(chalk.yellow('Přiřazuji obrázky k položkám...'));

        var total = 0;

        _.each(rows, function(row) {
            var productId = row.id_product;
            var imageId = row.id;
            var domain = row.domain;

            var index = _.findIndex(container, function(product) {
                return product.id == productId;
            });

            if (index === -1) {
                throw "Obrázek " + imageId + ": Nelze najít položku s ID " + row.id_product;
            }

            // skladani cesty k obrazku

            var imageUrl = 'http://' + domain + '/img/p/';

            for (var i = imageId.toString().length; i > 0; i--) {
                imageUrl += imageId.toString().substr(i * -1, 1) + '/';
            }

            imageUrl += imageId + '.jpg';

            var image = {};

            _.each(row, function(value, column) {
                if (!_.includes(schema.image.internalColumns, column)) {
                    image[column] = value;
                }
            });

            image.imageUrl = imageUrl;

            container[index].images = container[index].images || [];
            container[index].images.push(image);

            total++;
        });

        console.log(chalk.green('OK - Přiřazeno celkem ' + total + ' obrázků'));
    }).then(function() {
        var productIds = _.map(container, 'id');

        console.log(chalk.cyan('Načítám přiřazení produktů k vedlejším kategoriím...'));

        return knex.select('*', knex.column('id_category').as('id'))
            .from(tables.get('category_product'))
            .whereIn('id_product', productIds);

    }).then(function(rows) {
        _.each(rows, function(row) {
            var productId = row.id_product;
            var categoryId = row.id_category;

            var index = _.findIndex(container, function(product) {
                return product.id == productId;
            });

            if (index === -1) {
                throw "Kategorie " + categoryId + ": Nelze najít položku s ID " + productId;
            }

            var category = {default: false};

            if (container[index].id_category_default === categoryId) {
                category.default = true;

                delete container[index].id_category_default;
            }

            _.each(row, function(value, column) {
                if (!_.includes(schema.category.internalColumns, column)) {
                    category[column] = value;
                }
            });

            container[index].categories = container[index].categories || [];
            container[index].categories.push(category);
        });

        console.log(chalk.green('OK - vedlejší kategorie přiřazeny'));
    }).then(function() {
        return container;
    });
};