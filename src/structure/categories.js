// vyexportuje strukturu kategorii
var tables = require('../tables');
var chalk = require('chalk');
var _ = require('lodash');

var schema = {
    category: {
        langableColumns: [
            'name',
            'description'
        ],

        internalColumns: [
            'link_rewrite'
        ]
    }
};

var unflatten = function(array, parent, tree) {
    tree = tree || [];
    parent = parent || { id: 0 };

    var children = _.filter(array, function(child) {
        return child.id_parent == parent.id;
    });

    if (!_.isEmpty(children)) {
        if (parent.id == 0) {
            tree = children;
        } else {
            parent['children'] = children;
        }
        _.each(children, function(child) {
            unflatten(array, child)
        });
    }

    return tree;
};

module.exports = function(knex, prefix) {
    tables = tables(prefix);

    var query = knex.select(['id_category', 'level_depth'])
        .from(tables.get('category'))
        .where('level_depth', '=', 0)
        .orWhere('is_root_category', '=', 1);

    var invalidCategories;

    return query.then(function(rows) {

        var max = _.maxBy(rows, 'level_depth').level_depth;
        invalidCategories = _.map(rows, 'id_category');

        console.log(chalk.cyan.bold('\nNačítám strukturu kategorií...'));

        return knex.select(_.flatten([
                knex.column('c.id_category').as('id'),
                'c.id_parent', 'c.date_add', 'c.date_upd', 'c.position',
                'cl.link_rewrite',
                schema.category.langableColumns.map(function(item) {
                    return 'cl.' + item;
                }),
                knex.column('l.iso_code').as('lang_code'),
                knex.raw('c.level_depth - ' + max + ' as level_depth')
            ]))
            .where('level_depth', '!=', 0)
            .where('is_root_category', '=', 0)
            .from(tables.get('category', 'c'))
            .innerJoin(tables.get('category_lang', 'cl'), 'c.id_category', 'cl.id_category')
            .innerJoin(tables.get('lang', 'l'), 'cl.id_lang', 'l.id_lang');

    }).then(function(rows) {
        var parsed = [];

        var total = 0;

        _.each(rows, function(row) {

            var url = row.lang_code + '/' + row.id + '-' + row.link_rewrite;

            if (_.includes(invalidCategories, row.id_parent)) {
                row.id_parent = 0;
            }

            var existing = _.findIndex(parsed, function(category) {
                return category.id == row.id;
            });

            if (existing !== -1) {
                _.each(schema.category.langableColumns, function(column) {
                    parsed[existing][column][row.lang_code] = row[column];
                });

                parsed[existing].url[row.lang_code] = url;

            } else {
                var category = {url: {}};

                _.each(row, function(value, column) {
                    if (_.includes(schema.category.langableColumns, column)) {
                        category[column] = {};
                        category[column][row.lang_code] = value;
                    } else if (!_.includes(schema.category.internalColumns, column)) {
                        category[column] = value;
                    }
                });

                category.url[row.lang_code] = url;

                parsed.push(category);
                total++;
            }
        });

        console.log(chalk.green('OK - Nalezeno celkem ' + total + ' kategorií'));

        return unflatten(parsed);
    })
};
