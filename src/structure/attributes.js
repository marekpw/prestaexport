// vyexportuje strukturu atribut
var tables = require('../tables');
var chalk = require('chalk');
var _ = require('lodash');

var schema = {
    attributeGroup: {
        langableColumns: [
            'name',
            'public_name'
        ],

        internalColumns: [
            'lang_code'
        ]
    },

    attributeValue: {
        langableColumns: [
            'name'
        ],

        internalColumns: [
            'id_attribute_group',
            'lang_code'
        ]
    }
};

module.exports = function(knex, prefix) {
    tables = tables(prefix);

    console.log(chalk.cyan.bold('\nNačítám obecné informace o skupinách atributů...'));

    // data buffer
    var container = [];

    var main = knex.select(_.flatten([
        knex.column('ag.id_attribute_group').as('id'),
        'ag.is_color_group', 'ag.group_type', 'ag.position',
        schema.attributeGroup.langableColumns.map(function(item) {
            return 'agl.' + item;
        }), knex.column('l.iso_code').as('lang_code')
        ]))
        .from(tables.get('attribute_group', 'ag'))
        .innerJoin(tables.get('attribute_group_lang', 'agl'), 'agl.id_attribute_group', 'ag.id_attribute_group')
        .innerJoin(tables.get('lang', 'l'), 'agl.id_lang', 'l.id_lang');

    return main.then(function(rows) {
        var total = 0;

        _.each(rows, function(row) {
            var existing = _.findIndex(container, function(group) {
                return group.id == row.id;
            });

            if (existing !== -1) {
                _.each(schema.attributeGroup.langableColumns, function(column) {
                    container[existing][column][row.lang_code] = row[column];
                });
            } else {
                var group = {};

                _.each(row, function(value, column) {
                    if (_.includes(schema.attributeGroup.langableColumns, column)) {
                        group[column] = {};
                        group[column][row.lang_code] = value;
                    } else if (!_.includes(schema.attributeGroup.internalColumns, column)) {
                        group[column] = value;
                    }
                });

                container.push(group);
                total++;
            }
        });

        console.log(chalk.green('OK - Nalezeno celkem ' + total + ' skupin atributů'));
    }).then(function() {
        var attributeGroups = _.map(container, 'id');

        console.log(chalk.cyan('Načítám hodnoty jednotlivých atributů...'));

        return knex.select(_.flatten([
                knex.column('a.id_attribute').as('id'),
                'a.id_attribute_group', 'a.color', 'a.position',
                schema.attributeValue.langableColumns.map(function (item) {
                    return 'al.' + item;
                }), knex.column('l.iso_code').as('lang_code')
            ]))
            .from(tables.get('attribute', 'a'))
            .whereIn('a.id_attribute_group', attributeGroups)
            .innerJoin(tables.get('attribute_lang', 'al'), 'al.id_attribute', 'a.id_attribute')
            .innerJoin(tables.get('lang', 'l'), 'al.id_lang', 'l.id_lang')

    }).then(function(rows){
        console.log(chalk.yellow('Přiřazuji atributy ke skupinám...'));

        var total = 0;

        _.each(rows, function(row) {
            var groupId = row.id_attribute_group;
            var id = row.id;

            var index = _.findIndex(container, function(group) {
                return group.id == groupId;
            });

            if (index === -1) {
                throw "Atributa " + id + ": Nelze najít skupinu s ID " + groupId;
            }

            container[index].values = container[index].values || [];

            var existing = _.findIndex(container[index].values, function(e) {
                return e.id == id;
            });

            if (existing !== -1) {
                _.each(schema.attributeValue.langableColumns, function(column) {
                    container[index].values[existing][column][row.lang_code] = row[column];
                });
            } else {

                var attribute = {};

                _.each(row, function (value, column) {
                    if (_.includes(schema.attributeValue.langableColumns, column)) {
                        attribute[column] = {};
                        attribute[column][row.lang_code] = value;
                    } else if (!_.includes(schema.attributeValue.internalColumns, column)) {
                        attribute[column] = value;
                    }
                });

                container[index].values.push(attribute);
            }

            total++;
        });

        console.log(chalk.green('OK - Přiřazeno celkem ' + total + ' atribut ke skupinám'));
    }).then(function() {
        return container;
    });
};