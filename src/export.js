
var knex = require('knex');
var jsonfile = require('jsonfile');
var chalk = require('chalk');

var products = require('./structure/products');
var categories = require('./structure/categories');
var attributes = require('./structure/attributes');

var createConnection = function(db) {
    var connection = knex({
        client: 'mysql',
        connection: db
    });

    // musime volat v poradi, aby to fungovalo
    return connection.raw('SET NAMES utf8').then(function() {
        return connection.raw('SET CHARACTER SET utf8');
    }).then(function() {
        return connection;
    });
};

var exportHandler = function(db, prefix, output) {

    var structure = {};
    var connection;

    return createConnection(db).then(function(c) {
        // pripojeni k DB
        connection = c;
    }).then(function() {
        // pote nacteme prvni data
        return categories(connection, prefix);
    }).then(function(categories) {
        // pote pridame do struktury a nacteme dalsi
        structure.categories = categories;

        return attributes(connection, prefix);
    }).then(function(groups) {
        // pote pridame do struktury a nacteme dalsi
        structure.attribute_groups = groups;

        return products(connection, prefix);
    }).then(function(products) {
        // pridame do struktury
        structure.products = products;
    }).then(function() {
        // zapiseme do souboru
        jsonfile.writeFileSync(output, structure, {
            spaces: 4
        });

        // hotovo
        console.log(chalk.magenta.bold.inverse('\nHotovo!'));
        console.log(chalk.green.bold.inverse('\nZapsáno do souboru /tmp/products.json!'));
    });
};


module.exports = {
    export: exportHandler
};